﻿// MoveToClickPoint.cs
using UnityEngine;
using UnityEngine.AI;


//This is my pursuit function which has a couple functions.





public class Pursuit : MonoBehaviour
{
    public Transform target;
    public float maxvelocity = 3;
    public float mass = 15;
    private Vector3 velocity;
    public float maxforce = 15;


    //Start simply instantiates

    void Start()
    {
        velocity = Vector3.zero;
    }


    //Update uses a seek function with a purs helper to tracl down the target and go to that position

    void Update()
    {
        var desiredVelocity = target.transform.position - transform.position;
        desiredVelocity = desiredVelocity.normalized * maxvelocity;

        var steering = purs(target);
        steering = Vector3.ClampMagnitude(steering, maxforce);
        steering /= mass;
        velocity = Vector3.ClampMagnitude(velocity + steering, maxvelocity);
    
        transform.position += velocity * Time.deltaTime;
    }
    

    //Seek uses clamped vectors and vel to do an actualy seek function

    Vector3 seek(Vector3 position)
    {
        var vel = position - transform.position;
        vel = vel.normalized * maxvelocity;

        var steering = vel - velocity;
        steering = Vector3.ClampMagnitude(steering, maxforce);
        steering /= mass;
        velocity = Vector3.ClampMagnitude(velocity + steering, maxvelocity);
        position += velocity;
        return position;
    }


    //purs does the extra pursuit part of the business

    Vector3 purs(Transform t)
    {
        int T = 20;
        Vector3 futurePos = t.position + (t.position - transform.position);
            return seek(futurePos);

    }
}
