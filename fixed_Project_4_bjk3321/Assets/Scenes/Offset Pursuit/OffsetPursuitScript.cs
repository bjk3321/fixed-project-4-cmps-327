﻿// MoveToClickPoint.cs
using UnityEngine;
using UnityEngine.AI;


//This is actually just my pursuit function with an if statement attached to the Update function checking for a proximity to target.

public class OffsetPursuitScript : MonoBehaviour
{
    public Transform target;
    public float maxvelocity = 3;
    public float mass = 15;
    private Vector3 velocity;
    public float maxforce = 15;


    void Start()
    {
        velocity = Vector3.zero;
    }

    void Update()
    {
        var desiredVelocity = target.transform.position - transform.position;
        desiredVelocity = desiredVelocity.normalized * maxvelocity;

        var steering = purs(target);
        steering = Vector3.ClampMagnitude(steering, maxforce);
        steering /= mass;
        velocity = Vector3.ClampMagnitude(velocity + steering, maxvelocity);

        //THIS RIGHT HERE IS THE ONLY BIG CHANGE WHICH MAKES IT OFFSET PURSUIT
        if (Vector3.Distance(transform.position, target.transform.position) > 3.0f)
            transform.position += velocity * Time.deltaTime;
    }



    Vector3 seek(Vector3 position)
    {
        var dvel = position - transform.position;
        dvel = dvel.normalized * maxvelocity;

        var steering = dvel - velocity;
        steering = Vector3.ClampMagnitude(steering, maxforce);
        steering /= mass;
        velocity = Vector3.ClampMagnitude(velocity + steering, maxvelocity);
        position += velocity;
        return position;
    }

    Vector3 purs(Transform t)
    {
        int T = 20;
        Vector3 futurePos = t.position + (t.position - transform.position);
            return seek(futurePos);

    }
}
