﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script that makes a target rotate in a circle. This is used to give the actual testing scripts something to pursue or flee from.
public class Circle : MonoBehaviour {
    public float speed = 3;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += transform.forward * speed;
        transform.Rotate(0f, 3f, 0f);
	}
}
