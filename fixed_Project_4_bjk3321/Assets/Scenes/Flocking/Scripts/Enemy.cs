﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Just a member function with separate wandering

public class Enemy : Member {

    protected override Vector3 Flocking()
    {
        return conf.wanderPriority * Wander();
    }
}
