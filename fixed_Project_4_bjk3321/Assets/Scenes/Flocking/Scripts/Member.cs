﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Member : MonoBehaviour {

    //Main Script for Flocking AND Wander behaviour

    public Vector3 position;
    public Vector3 velocity;
    public Vector3 acceleration;

    public Level level;
    public MemberConfig conf;

    Vector3 wanderTarget;


    //Iniatializes
    void Start() {
        level = FindObjectOfType<Level>();
        conf = FindObjectOfType<MemberConfig>();

        position = transform.position;
        velocity = new Vector3(Random.Range(-3, 3), Random.Range(-3,3), 0);
    }

    //Calls major functions in one large combine function then clamps thier magnitudes
    
    private void Update()
    {
        acceleration = Flocking();
        acceleration = Vector3.ClampMagnitude(acceleration, conf.maxAcceleration);
        velocity = velocity + acceleration * Time.deltaTime;
        velocity = Vector3.ClampMagnitude(velocity, conf.maxVelocity);
        position = position + velocity * Time.deltaTime;
        WrapAround(ref position, -level.bounds, level.bounds);
        transform.position = position;
    }


    //Wander function for the bonus, thought it would be cool if the flockers also implemented wander
    //I ran into an error where all of my members and enemies would constantly be locked onto a single axis and I think the error is here.

    protected Vector3 Wander() {
        float jitter = conf.wanderJitter * Time.deltaTime;
        wanderTarget += new Vector3(RandomBionomial() * jitter, RandomBionomial() * jitter, 0);
        wanderTarget = wanderTarget.normalized;
        wanderTarget *= conf.wanderRadius;
        Vector3 targetInLocalSpace = wanderTarget + new Vector3(RandomBionomial(), RandomBionomial(), 0);
        Vector3 targetInWorldSpace = transform.TransformPoint(targetInLocalSpace);
        targetInWorldSpace -= this.position;
        return targetInWorldSpace.normalized;
    }


   

    Vector3 Cohesion() {
        Vector3 cohesionVector = new Vector3();
        int countMembers = 0;
        var neighbors = level.GetNeighbors(this, conf.cohesionRadius);
        if (neighbors.Count == 0)
            return cohesionVector;
        foreach (var member in neighbors) {
            if (isInFov(member.position)) {
                cohesionVector += member.position;
            }
        }
        if (countMembers == 0)
            return cohesionVector;

        cohesionVector /= countMembers;
        cohesionVector = cohesionVector - this.position;
        cohesionVector = Vector3.Normalize(cohesionVector);
        return cohesionVector;
    }


    //Alignment function for implementing Flocking

    Vector3 Alignment() {
        Vector3 alignVector = new Vector3();
        var members = level.GetNeighbors(this, conf.alignmentRadius);
        if (members.Count == 0)
            return alignVector;
        foreach (var member in members) {
            if (isInFov(member.position))
                alignVector += member.velocity;
        }

        return alignVector.normalized;
    }


    //Separation function for implementing Flocking

    Vector3 Separation() {
        Vector3 separationVector = new Vector3();
        var members = level.GetNeighbors(this, conf.separationRadius);
        if (members.Count == 0)
            return separationVector;
        foreach (var member in members) {
            if (isInFov(member.position))
            {
                Vector3 movingTowards = this.position - member.position;
                if (movingTowards.magnitude > 0) {
                    separationVector += movingTowards.normalized / movingTowards.magnitude;
                }
            }
        }

        return separationVector.normalized;
    }


    //Extra Avoidance function for implementing avoiding enemies

    Vector3 Avoidance() {
        Vector3 avoidVector = new Vector3();
        var enemyList = level.GetEnemies(this, conf.avoidanceRadius);
        if (enemyList.Count == 0)
            return avoidVector;
        foreach (var enemy in enemyList) {
            avoidVector += RunAway(enemy.position);
        }

        return avoidVector.normalized;
    }


    //Helper function for Avoidance
    Vector3 RunAway(Vector3 target) {
        Vector3 neededVelocity = (position - target.normalized * conf.maxVelocity);
        return neededVelocity - velocity;
    }


    //MASSIVE Flocking Function call that calls all other functions above
    virtual protected Vector3 Flocking() {
        Vector3 finalVec = conf.cohesionPriority * Cohesion() + conf.wanderPriority * Wander()
            + conf.alignmentPriority * Alignment() + conf.separationPriority * Separation()
            + conf.avoidancePriority * Avoidance();
        return finalVec;
    }


    //I attempted to get my members and enemies to wrap around for a looping behaviour
    void WrapAround(ref Vector3 vector, float min, float max) {
        vector.x = WrapAroundFloat(vector.x, min, max);
        vector.y = WrapAroundFloat(vector.y, min, max);
        vector.z = WrapAroundFloat(vector.z, min, max);
    }


    //Helper function for wrap around
    float WrapAroundFloat(float value, float min, float max) {
        if (value > max)
            value = min;
        else if (value < min)
            value = max;
        return value;
    }


    //Helper function to create randomness 
    float RandomBionomial() {
        return Random.Range(0f, 1f) - Random.Range(0f, 1f);
    }


    //Helper function to check the proximity of another nearby member
    bool isInFov(Vector3 vec) {
        return Vector3.Angle(this.velocity, vec - this.position) <= conf.maxFOV;
    }
}
